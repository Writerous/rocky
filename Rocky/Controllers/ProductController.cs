﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Rocky.Data;
using Rocky.Models;
using Rocky.Models.ViewModels;

namespace Rocky.Controllers
{
    public class ProductController : Controller
    {
        private readonly ApplicationDbContext _db;

        public ProductController(ApplicationDbContext db)
        {
            _db = db;
        }
        
        // GET products list page
        public IActionResult Index()
        {
            IEnumerable<Product> products = _db.Product;
            
            return View(products);
        }

        // GET product page
        public IActionResult Upsert(int? id)
        {
            var productVm = new ProductVM
            {
                Product = new Product(),
                CategorySelectList = _db.Category.Select(category => new SelectListItem
                {
                    Text = category.Name,
                    Value = category.Id.ToString()
                })
            };
                
            if (id == null)
            {
                return View(productVm);
            }
            productVm.Product = _db.Product.Find(id);
            if (productVm.Product == null)
            {
                return NotFound();
            }
            return View(productVm);
        }

        // POST new or existing product
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Upsert(ProductVM productVm)
        {
            if (ModelState.IsValid)
            {
                return RedirectToAction("Index");
            }

            return View(productVm);
        }

        public IActionResult Delete(int? id)
        {
            return View();
        }
    }
}