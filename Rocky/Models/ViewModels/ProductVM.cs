﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace Rocky.Models.ViewModels
{
    public class ProductVM
    {
        public IEnumerable<SelectListItem> CategorySelectList { get; set; }
        public Product Product { get; set; }
    }
}