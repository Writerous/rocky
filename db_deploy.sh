﻿#!/usr/bin/env bash
docker run \
  --name rocky-db \
  -e "ACCEPT_EULA=Y" \
  -e "SA_PASSWORD=Password123" \
  -p 1434:1433 \
  --mount type=bind,source="$(pwd)"/rocky_db_volume,target=/var/opt/mssq \
  -d \
   mcr.microsoft.com/mssql/server \
   